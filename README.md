## Exchange rate asynchronius service
Зависимости
```
Для работы приложения необходима версия языка Python 3.7 и выше 
```
Установка python package
```
python setup.py install
```
Сборка docker образа и запуск приложения

```
docker-compose build
```
```
docker-compose up --remove-orphans --exit-code-from=app app.
```
или
```
./run_app.sh
```

URL
```bash
http://0.0.0.0:8000/exchange
```
Запуск тестов 
```
./run_tests.sh
```