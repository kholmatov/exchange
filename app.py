import os
import logging

import argparse
from aiomisc import entrypoint

from services.exchange import ExchangePeriodicService
from services.service import REST


def parse_args():
    parser = argparse.ArgumentParser()
    group = parser.add_argument_group('HTTP options')
    group.add_argument("-l", "--address", default="0.0.0.0",
                       help="Listen HTTP address")
    group.add_argument("-p", "--port", type=int, default=8000,
                       help="Listen HTTP port")

    args = parser.parse_args()
    os.environ.clear()
    return args


arguments = parse_args()

services = (
    ExchangePeriodicService(interval=10),
    REST(address=arguments.address, port=arguments.port)
)
with entrypoint(*services,
                pool_size=2,
                log_level='info',
                log_format='json',
                log_buffer_size=1024,
                log_flush_interval=0.2,
                debug=False,
                log_config=True
                ) as loop:
    logging.info("Server is running on http://%s:%s", arguments.address, arguments.port)
    loop.run_forever()
