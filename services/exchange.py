import asyncio
import logging
import aiohttp
import time
from aiomisc.service.periodic import PeriodicService


class ExchangePeriodicService(PeriodicService):

    async def set(self, value):
        self.context['var'] = {
            'parser_time': time.strftime("%Y-%m-%d %H:%M:%S"),
            'result_data': value
        }

    async def get(self):
        return self.context['var']

    async def fetch(self, session, url):
        while True:
            try:
                async with session.get(url) as response:
                    logging.info("Parsing data from the site: %s", url)
                    return await response.json()
            except aiohttp.ClientError as e:
                logging.debug("Something is wrong with: %s", e)
                await asyncio.sleep(0.5)

    async def callback(self):
        async with aiohttp.ClientSession() as session:
            await self.set(
                await self.fetch(session, 'https://api.ratesapi.io/api/latest')
            )
