import logging

import aiohttp.web
from aiomisc.service.aiohttp import AIOHTTPService
from aiomisc import get_context


async def handle(request):
    logging.info("Reply to the request /exchange")
    context = get_context()
    return aiohttp.web.json_response(data=await context['var'])


class REST(AIOHTTPService):
    async def create_application(self):
        app = aiohttp.web.Application()

        app.add_routes([
            aiohttp.web.get('/exchange', handle)
        ])

        return app
