# base image
FROM snakepacker/python:all as builder

COPY requirements*.txt /tmp/
RUN python3.7 -m venv /usr/share/python3/app
RUN python3.7 -m venv /usr/share/python3/tests
RUN /usr/share/python3/app/bin/pip install --no-cache-dir -Ur /tmp/requirements.txt
RUN /usr/share/python3/tests/bin/pip install --no-cache-dir -Ur /tmp/requirements.tests.txt

########################################################################
# CI image
FROM snakepacker/python:3.7 as tests

COPY --from=builder /usr/share/python3/tests /usr/share/python3/tests
COPY services/ /mnt/services/
COPY tests/ /mnt/tests/

ENV PATH="/usr/share/python3/tests/bin:${PATH}"
WORKDIR /mnt

########################################################################
# Image with app installed
FROM snakepacker/python:3.7 as app

COPY --from=builder /usr/share/python3/app /usr/share/python3/app
COPY services/ /mnt/services/
COPY app.py /mnt/app.py

ENV PATH="/usr/share/python3/app/bin:${PATH}"
SHELL ["/bin/bash", "-c"]
WORKDIR /mnt
EXPOSE 8000

CMD ["python3", "app.py"]

