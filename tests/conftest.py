import os
import json
import pytest

from services.service import REST

TESTS_DIR = os.path.dirname(__file__)


@pytest.fixture
def exchange_data():
    path = os.path.join(TESTS_DIR, 'data/test_dump.json')
    with open(path) as f:
        return json.load(f)['result_data']


@pytest.fixture
def demo_port(aiomisc_unused_port_factory):
    return aiomisc_unused_port_factory()


@pytest.fixture
def demo_host():
    return "127.0.0.1"


@pytest.fixture
def demo_service(demo_host, demo_port):
    return REST(address=demo_host, port=demo_port)


@pytest.fixture
def services(demo_service):
    return [demo_service]
