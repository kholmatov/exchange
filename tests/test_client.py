import aiohttp
from services.exchange import ExchangePeriodicService
from asynctest import patch, asyncio
from aiomisc import get_context
from http import HTTPStatus


# test periodic service
def test_str_representation():
    svc = ExchangePeriodicService(interval=10, delay=0)
    assert str(svc) == 'ExchangePeriodicService(interval=10,delay=0)'


# mocked ExchangePeriodicService
async def test_exchange_period_func(exchange_data):
    with patch('services.exchange.ExchangePeriodicService.fetch',
               return_value=exchange_data) as fetch:
        loop = asyncio.get_event_loop()
        loop.create_task(
            ExchangePeriodicService.callback(
                ExchangePeriodicService(interval=1)
            )
        )
        await asyncio.sleep(0.5)
        context = get_context()
        context_result = await context['var']
        assert await fetch() == context_result['result_data']


# test Rest Server with mock
async def test_rest_func(exchange_data, demo_host, demo_port):
    async def get_response(sess, url):
        async with sess.get(url) as response:
            if response.content_type == 'application/json':
                return response, await response.json()
            else:
                return response

    with patch('services.exchange.ExchangePeriodicService.fetch',
               return_value=exchange_data):
        loop = asyncio.get_event_loop()
        loop.create_task(
            ExchangePeriodicService.callback(
                ExchangePeriodicService(interval=1)
            )
        )

        async with aiohttp.ClientSession() as session:
            result, data = await get_response(
                session,
                f"http://{demo_host}:{demo_port}/exchange"
            )
            assert (result.status == HTTPStatus.OK
                    and result.content_type == 'application/json')
            assert data['result_data'] == exchange_data

            result = await get_response(
                session, f"http://{demo_host}:{demo_port}/not_found"
            )
            assert (result.status == HTTPStatus.NOT_FOUND
                    and result.content_type == 'text/plain')
