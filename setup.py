from setuptools import setup

setup(
    name='exchange',
    version='0.0.1',
    packages=['tests', 'services'],
    url='http://i-taj.com',
    license='MIT',
    author='Erkin Kholmatov',
    author_email='kholmatov@yandex.ru',
    description='Asynchronius get exchange rate service'
)
